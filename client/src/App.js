import React, { Component } from 'react';
import './App.css';

class App extends Component {
  state = {
    records: []
  }

  componentDidMount() {
    fetch('http://localhost:8000/api/device_logs')
    .then(res => res.json())
    .then(records => this.setState({ records: records }))
  }

  render() {
    const { records } = this.state

    return (
      <div className="App">
        <table>
          <thead>
            <tr>
              <th>ID</th>
              <th>Data</th>
            </tr>
          </thead>
          <tbody>
            { records.map( record => {
              return (
                <tr key={record.id}>
                  <td>{ record.id }</td>
                  <td>{ record.data }</td>
                </tr>
              )
            }) }
          </tbody>
        </table>
      </div>
    );
  }
}

export default App;
