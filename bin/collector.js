const http = require('http')
const app = require('../log_collector')

const port = parseInt(process.env.PORT, 10) || 9000
app.set('port', port)

const server = http.createServer(app)
server.listen(port)
