process.env.NODE_ENV = 'test'

const DeviceLogs = require('../server/models').DeviceLogs

const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../app')
const should = chai.should()
const expect = chai.expect

chai.use(chaiHttp)

describe('POST /push.do', () => {
  beforeEach((done) => {
    DeviceLogs
      .destroy({ where: {}, truncate: true })
      .then(done)
  })

  it('it should save payload', (done) => {
    let payload = '{"phoneNumber":"+ 79811915683","message":"$GPRMC,210640.000,A,5554.2687,N,03723.1046,E,0.54,356.82,031218,,,A* 34,09; GSM: 250-01 18d3-3e4b,6d52,78ac,b3c5,3ea0 13;  M; Batt: 398,R\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r"}'

    chai
      .request(server)
      .post('/push.do')
      .send(payload)
      .end(async (err, res) => {
        res.should.have.status(200)

        let count = await DeviceLogs.count()
        try {
          expect(count).to.equal(1)
          done()
        } catch(e) {
          done(e)
        }
      })
  })
})
