const express = require('express')
const logger = require('morgan')
const cors = require('cors')
const errorhandler = require('errorhandler')

const app = express()

app.use(logger('dev'))
app.use(cors())

if (process.env.NODE_ENV !== 'production') {
  app.use(errorhandler({ log: true }))
}

require('./server/routes/')(app)

module.exports = app
