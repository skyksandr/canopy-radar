const bodyParser = require('body-parser')
const deviceLogsController = require('../controllers').deviceLogs

module.exports = (app) => {
  app.get('/api/device_logs', deviceLogsController.list),
  app.post('/push.do', bodyParser.text({type: '*/*'}), deviceLogsController.create)
}
